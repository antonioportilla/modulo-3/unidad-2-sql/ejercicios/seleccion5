<?php

use yii\helpers\Html;

echo Html::a('Ciclistas',
        ['ciclista/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';

echo Html::a('Etapas',
        ['etapa/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';

echo Html::a('Equipos',
        ['equipo/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';

echo Html::a('Lleva',
        ['lleva/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';

echo Html::a('Maillot',
        ['maillot/index'],
        ['class' => 'btn btn-primary btn-large']
        );

echo ' ';

echo Html::a('Puerto',
        ['puerto/index'],
        ['class' => 'btn btn-primary btn-large']
        );



?>