<?php
use yii\bootstrap\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="site-index">

    <div class="jumbotron">
        <h1>consultas de seleccion 5</h1>
Application
        <p class="lead">Modulo 3 Unidad 2</p>
    </div>

    <div class="body-content">

        <div class="row">
            
             <!-- primera consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 1</h3>
                      <p>Nombre y edad de los ciclistas que no han ganado etapas.</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta1a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta1'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
             
              <!-- segunda consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 2</h3>
                      <p>Nombre y edad de los ciclistas que no han ganado puertos.</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta2a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta2'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
              <!-- tercera consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 3</h3>
                      <p>Listar el director de los equipos que tengan ciclistas NO que hayan ganado NINGUNA etapa.</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta3a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta3'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
              <!-- cuarta consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 4</h3>
                      <p>Dorsal y nombre de los ciclistas que no hayan llevado algún maillot.</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta4a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta4'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
              <!-- quinta consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 5</h3>
                      <p>Dorsal y nombre de los ciclistas que NO hayan llevado algún maillot amarillo.</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta5a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta5'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
              <!-- sexta consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 6</h3>
                      <p>Indicar el numetapa de las etapas que NO tengan puertos.</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta6a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta6'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
              <!-- septima consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 7</h3>
                      <p>Indicar La distancia media de las etapas que no tengan puertos.</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta7a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta7'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
              
              <!-- octava consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 8</h3>
                      <p>Listar el número de ciclists que NO hayan ganado alguna etapa.</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta8a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta8'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
              <!-- novena consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 9</h3>
                      <p>Listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerto.</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta9a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta9'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
              <!-- decima consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 10</h3>
                      <p>Listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puertos.</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta10a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta10'],['class' => 'btn btn-default btn-large']);?>
                          <?= Html::a('ListView',['site/consulta10b'],['class' => 'btn btn-default btn-large']);?>
                          <?= Html::a('Active Record2',['site/consulta10c'],['class' => 'btn btn-primary btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
                            
              
        </div>

    </div>
</div>
