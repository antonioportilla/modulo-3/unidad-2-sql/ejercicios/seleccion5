<div>
    <h1>Listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puertos.</h1>
    
</div>

<?php
use yii\widgets\ListView;
?>
<table border="5">
    <thead>
        <tr>
            <th>Dorsal</th>
        </tr>
    </thead>
      <tbody>
<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_listar',
]);
?>
  
    </tbody>
</table>


