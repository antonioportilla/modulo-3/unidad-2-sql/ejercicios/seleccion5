﻿USE ciclistas;

-- 1.1 Nombre y edad de los ciclistas que no han ganado etapas.
SELECT DISTINCT ciclista.nombre, ciclista.edad FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL;

-- 1.2 Nombre y edad de los ciclistas que no han ganado puertos.
SELECT DISTINCT ciclista.nombre, ciclista.edad FROM ciclista LEFT JOIN puerto ON ciclista.dorsal=puerto.dorsal WHERE puerto.dorsal IS NULL;

-- 1.3 Listar el director de los equipos que tengan ciclistas NO que hayan ganado NINGUNA etapa.
SELECT DISTINCT equipo.director FROM equipo INNER JOIN (ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal)  ON equipo.nomequipo=ciclista.nomequipo WHERE etapa.dorsal IS NULL;

-- 1.4 Dorsal y nombre de los ciclistas que no hayan llevado algún maillot.
SELECT DISTINCT ciclista.dorsal, ciclista.nombre FROM ciclista LEFT JOIN lleva ON ciclista.dorsal=lleva.dorsal WHERE lleva.dorsal IS NULL;


-- 1.5 Dorsal y nombre de los ciclistas que NO hayan llevado algún maillot amarillo.

SELECT DISTINCT lleva.dorsal FROM  lleva  INNER JOIN maillot ON lleva.código=maillot.código and color='amarillo';

SELECT ciclista.dorsal, ciclista.nombre FROM ciclista LEFT JOIN (SELECT DISTINCT lleva.dorsal FROM  lleva  INNER JOIN maillot ON lleva.código=maillot.código and color='amarillo')c1 ON ciclista.dorsal=c1.dorsal WHERE c1.dorsal IS NULL;








SELECT  DISTINCT ciclista.dorsal, ciclista.nombre FROM ciclista  LEFT JOIN lleva ON ciclista.dorsal=lleva.dorsal INNER JOIN maillot ON lleva.código=maillot.código AND maillot.color='amarillo' WHERE lleva.dorsal IS NULL;



-- 1.6 Indicar el numetapa de las etapas que NO tengan puertos.
SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL;
SELECT COUNT(*) cuenta_numetapa FROM (SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL) c1;

-- 1.7 Indicar La distancia media de las etapas que no tengan puertos.

SELECT AVG(etapa.kms) AS PromedioDekms FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL;

-- 1.8 Listar el número de ciclists que NO hayan ganado alguna etapa.

SELECT COUNT(*) FROM (SELECT ciclista.dorsal FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL) c1;

-- 1.9 Listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerto.

SELECT DISTINCT etapa.dorsal FROM  etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL;

-- 1.10 Listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puertos.

SELECT DISTINCT etapa.dorsal FROM  etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL; -- c1

SELECT DISTINCT etapa.dorsal FROM  etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa; -- c2

SELECT c1.dorsal FROM (SELECT DISTINCT etapa.dorsal FROM  etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL)c1 LEFT JOIN (SELECT DISTINCT etapa.dorsal FROM  etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa)c2 ON c1.dorsal=c2.dorsal WHERE c2.dorsal IS NULL;
-- quita del subconjunto ganadores de etapas que no tienen puertos los que coincidan con los ganadores que si tiene puertos, el único que coincide es el dorsal número 2.

-- otra solucion con una única subconsulta ya que resta del total un conjunto.

  SELECT DISTINCT etapa.dorsal FROM etapa LEFT JOIN (SELECT DISTINCT etapa.dorsal FROM  etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa) c1 ON etapa.dorsal=c1.dorsal WHERE c1.dorsal IS not NULL;


  