<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ciclista".
 *
 * @property int $dorsal
 * @property string $nombre
 * @property int $edad
 * @property string $nomequipo
 *
 * @property Equipo $nomequipo0
 * @property Etapa[] $etapas
 * @property Lleva[] $llevas
 * @property Puerto[] $puertos
 */
class Ciclista extends \yii\db\ActiveRecord
{
    public $nciclistas;
    public $edadMedia;
    public $km;
    public $numetapa0;
    public $nompuerto;
    public $num;
    public $direc;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ciclista';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dorsal', 'nombre', 'nomequipo'], 'required'],
            [['dorsal', 'edad'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['nomequipo'], 'string', 'max' => 25],
            [['dorsal'], 'unique'],
            [['nomequipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipo::className(), 'targetAttribute' => ['nomequipo' => 'nomequipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dorsal' => 'Dorsal',
            'nombre' => 'Nombre',
            'edad' => 'Edad',
            'nomequipo' => 'Nomequipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNomequipo0()
    {
        return $this->hasOne(Equipo::className(), ['nomequipo' => 'nomequipo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEtapas()
    {
        return $this->hasMany(Etapa::className(), ['dorsal' => 'dorsal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLlevas()
    {
        return $this->hasMany(Lleva::className(), ['dorsal' => 'dorsal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPuertos()
    {
        return $this->hasMany(Puerto::className(), ['dorsal' => 'dorsal']);
    }

}
