<?php

namespace app\controllers;
use yii\data\SqlDataProvider;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;
use yii\data\ActiveDataProvider;
use app\models\Equipo;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    /*
     * Creado esta accion
     */
    
    public function actionCrud(){
        return $this->render('gestion');
    }


     public function actionConsulta1a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            
            'query'=> Ciclista::find()->joinWith('etapas',true)->distinct()->select("nombre, edad")->where("etapa.dorsal is null")
                ,
            'pagination'=>[
                'pageSize'=>0,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'],
            "titulo"=>"Consulta 1",
            "enunciado"=>"Nombre y edad de los ciclistas que no han ganado etapas.",
            "sql"=>"SELECT DISTINCT ciclista.nombre, ciclista.edad FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL",
        ]);
    }
    
     public function actionConsulta1(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT DISTINCT ciclista.nombre, ciclista.edad FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL) c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT ciclista.nombre, ciclista.edad FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>0,
                ],
        ]);
        
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'],
            "titulo"=>"Consulta 1",
            "enunciado"=>"Nombre y edad de los ciclistas que no han ganado etapas.",
            "sql"=>"SELECT DISTINCT ciclista.nombre, ciclista.edad FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL",
        ]);


     }

    
     public function actionConsulta2a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            
            'query'=> Ciclista::find()->joinWith( 'puertos',true)->distinct()->select("nombre, edad")->where("puerto.dorsal is null")
                 ,
            'pagination'=>[
                'pageSize'=>30,
                ]
        ]);
                
        // $a=Ciclista::find()->innerJoinWith( 'puertos',true,'inner join')->all();
        // var_dump($a[0]->puertos); // innerJoinWith utiliza los modelos que ya est´n relacionados 
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'],
            "titulo"=>"Consulta 2",
            "enunciado"=>"Nombre y edad de los ciclistas que no han ganado puertos.",
            "sql"=>"SELECT DISTINCT c.nombre, c.edad FROM puerto p INNER JOIN ciclista c ON c.dorsal=p.dorsal",
        ]);
    }
    
     public function actionConsulta2(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT DISTINCT ciclista.nombre, ciclista.edad FROM ciclista LEFT JOIN puerto ON ciclista.dorsal=puerto.dorsal WHERE puerto.dorsal IS NULL) c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT ciclista.nombre, ciclista.edad FROM ciclista LEFT JOIN puerto ON ciclista.dorsal=puerto.dorsal WHERE puerto.dorsal IS NULL" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>30,
                ],
        ]);
        
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'],
            "titulo"=>"Consulta 2",
            "enunciado"=>"Nombre y edad de los ciclistas que no han ganado puertos.",
            "sql"=>"SELECT DISTINCT ciclista.nombre, ciclista.edad FROM ciclista LEFT JOIN puerto ON ciclista.dorsal=puerto.dorsal WHERE puerto.dorsal IS NULL",
        ]);


     }
     
     
          public function actionConsulta3a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            
            'query'=> Ciclista::find()->joinWith( 'etapas',true)->innerJoin('equipo','equipo.nomequipo=ciclista.nomequipo')->distinct()->select("equipo.director as direc")->where("etapa.dorsal IS NULL")
                 ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
        // $a=Ciclista::find()->innerJoinWith( 'puertos',true,'inner join')->all();
        // var_dump($a[0]->puertos); // innerJoinWith utiliza los modelos que ya est´n relacionados 
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['direc'],
            "titulo"=>"Consulta 3",
            "enunciado"=>"Listar el director de los equipos que tengan ciclistas NO que hayan ganado NINGUNA etapa.",
            "sql"=>"SELECT DISTINCT equipo.director FROM equipo INNER JOIN (ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal)  ON equipo.nomequipo=ciclista.nomequipo WHERE etapa.dorsal IS NULL",
        ]);
    }
    
     public function actionConsulta3(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT DISTINCT equipo.director FROM equipo INNER JOIN (ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal)  ON equipo.nomequipo=ciclista.nomequipo WHERE etapa.dorsal IS NULL) c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT equipo.director FROM equipo INNER JOIN (ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal)  ON equipo.nomequipo=ciclista.nomequipo WHERE etapa.dorsal IS NULL" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
            return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['director'],
            "titulo"=>"Consulta 3",
            "enunciado"=>"Listar el director de los equipos que tengan ciclistas NO que hayan ganado NINGUNA etapa.",
            "sql"=>"SELECT DISTINCT equipo.director FROM equipo INNER JOIN (ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal)  ON equipo.nomequipo=ciclista.nomequipo WHERE etapa.dorsal IS NULL",
        ]);


     }
    
    
    public function actionConsulta4a(){
        
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->JoinWith('llevas',true)->distinct()->select("ciclista.dorsal, nombre")->where("lleva.dorsal IS NULL")
                         ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 4",
            "enunciado"=>"Dorsal y nombre de los ciclistas que no hayan llevado algún maillot.",
            "sql"=>"SELECT DISTINCT ciclista.dorsal, ciclista.nombre FROM ciclista LEFT JOIN lleva ON ciclista.dorsal=lleva.dorsal WHERE lleva.dorsal IS NULL",
        ]);
    }
    
     public function actionConsulta4(){
            // mediante DAO
        
         $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT DISTINCT ciclista.dorsal, ciclista.nombre FROM ciclista LEFT JOIN lleva ON ciclista.dorsal=lleva.dorsal WHERE lleva.dorsal IS NULL) c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT ciclista.dorsal, ciclista.nombre FROM ciclista LEFT JOIN lleva ON ciclista.dorsal=lleva.dorsal WHERE lleva.dorsal IS NULL" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 4",
            "enunciado"=>"Dorsal y nombre de los ciclistas que no hayan llevado algún maillot.",
            "sql"=>"SELECT DISTINCT ciclista.dorsal, ciclista.nombre FROM ciclista LEFT JOIN lleva ON ciclista.dorsal=lleva.dorsal WHERE lleva.dorsal IS NULL",
        ]);
    }
    
    public function actionConsulta5a(){
        
        $subconsulta=Lleva::find()->innerJoin ('maillot',"lleva.código=maillot.código")->distinct()->select("dorsal")->where("maillot.color='amarillo'");
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->leftJoin (['a'=>$subconsulta],'ciclista.dorsal=a.dorsal')->distinct()->select("ciclista.dorsal, nombre")->where("a.dorsal IS NULL")
                ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 5",
            "enunciado"=>"Dorsal y nombre de los ciclistas que NO hayan llevado algún maillot amarillo.",
            "sql"=>"SELECT ciclista.dorsal, ciclista.nombre FROM ciclista LEFT JOIN (SELECT DISTINCT lleva.dorsal FROM  lleva  INNER JOIN maillot ON lleva.código=maillot.código and color='amarillo')c1 ON ciclista.dorsal=c1.dorsal WHERE c1.dorsal IS NULL",
        ]);
    }
    
     public function actionConsulta5(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT ciclista.dorsal, ciclista.nombre FROM ciclista LEFT JOIN (SELECT DISTINCT lleva.dorsal FROM  lleva  INNER JOIN maillot ON lleva.código=maillot.código and color='amarillo')c1 ON ciclista.dorsal=c1.dorsal WHERE c1.dorsal IS NULL) c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT ciclista.dorsal, ciclista.nombre FROM ciclista LEFT JOIN (SELECT DISTINCT lleva.dorsal FROM  lleva  INNER JOIN maillot ON lleva.código=maillot.código and color='amarillo')c1 ON ciclista.dorsal=c1.dorsal WHERE c1.dorsal IS NULL" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>20,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal','nombre'],
            "titulo"=>"Consulta 5",
            "enunciado"=>"Dorsal y nombre de los ciclistas que NO hayan llevado algún maillot amarillo.",
            "sql"=>"SELECT ciclista.dorsal, ciclista.nombre FROM ciclista LEFT JOIN (SELECT DISTINCT lleva.dorsal FROM  lleva  INNER JOIN maillot ON lleva.código=maillot.código and color='amarillo')c1 ON ciclista.dorsal=c1.dorsal WHERE c1.dorsal IS NULL",
        ]);
    }
    
    
        public function actionConsulta6a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Etapa::find()->leftJoin ('puerto','etapa.numetapa=puerto.numetapa')->distinct()->select("etapa.numetapa")->where("puerto.numetapa IS NULL")
                ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 6",
            "enunciado"=>"Indicar el numetapa de las etapas que NO tengan puertos.",
            "sql"=>"SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
    }
    
     public function actionConsulta6(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL) c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'],
            "titulo"=>"Consulta 6",
            "enunciado"=>"Indicar el numetapa de las etapas que NO tengan puertos.",
            "sql"=>"SELECT DISTINCT etapa.numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
    }
    
            public function actionConsulta7a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Etapa::find()->JoinWith ('puertos',true)->select("AVG(etapa.kms) PromedioDekms")->where("puerto.numetapa IS NULL")
                ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['PromedioDekms'],
            "titulo"=>"Consulta 7",
            "enunciado"=>"Indicar La distancia media de las etapas que no tengan puertos.",
            "sql"=>"SELECT AVG(etapa.kms) AS PromedioDekms FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
    }
    
     public function actionConsulta7(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT 1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT AVG(etapa.kms) AS PromedioDekms FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['PromedioDekms'],
            "titulo"=>"Consulta 7",
            "enunciado"=>"Indicar La distancia media de las etapas que no tengan puertos.",
            "sql"=>"SELECT AVG(etapa.kms) AS PromedioDekms FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
    }
    
    public function actionConsulta8a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->joinWith('etapas',true)->distinct()->select("count(ciclista.dorsal) dorsal")->where("etapa.dorsal IS NULL")
                ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8",
            "enunciado"=>"Listar el número de ciclists que NO hayan ganado alguna etapa.",
            "sql"=>"SELECT DISTINCT puerto.numetapa FROM puerto",
        ]);
    }
    
     public function actionConsulta8(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT 1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT COUNT(*) as total FROM (SELECT ciclista.dorsal FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL) c1" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>0,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 8",
            "enunciado"=>"Listar el número de ciclists que NO hayan ganado alguna etapa.",
            "sql"=>"SELECT COUNT(*) as total FROM (SELECT ciclista.dorsal FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal WHERE etapa.dorsal IS NULL) c1",
        ]);
    } 
    
        public function actionConsulta9a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Etapa::find()->joinWith('puertos',true)->distinct()->select("etapa.dorsal")->where("puerto.numetapa IS NULL")
                ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 9",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerto.",
            "sql"=>"SELECT DISTINCT etapa.dorsal FROM  etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
    }
    
     public function actionConsulta9(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT COUNT(*) FROM (SELECT DISTINCT etapa.dorsal FROM  etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL) c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT etapa.dorsal FROM  etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 9",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerto.",
            "sql"=>"SELECT DISTINCT etapa.dorsal FROM  etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
    } 
 
    
         public function actionConsulta10a(){
        // mediante active record
             
        $r=Etapa::find()->innerJoin('puerto','etapa.numetapa=puerto.numetapa')->distinct()->select("etapa.dorsal");
        
        $dataProvider=new ActiveDataProvider([
            'query'=>Etapa::find()->distinct()->select('dorsal')->leftJoin(['c1'=>$r],"etapa.dorsal=c1.dorsal")->select("etapa.dorsal")->where("c1.dorsal is  null") ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
  
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puertos.",
            "sql"=>"SELECT DISTINCT etapa.dorsal FROM etapa LEFT JOIN (SELECT DISTINCT etapa.dorsal FROM  etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa) c1 ON etapa.dorsal=c1.dorsal WHERE c1.dorsal IS NULL",
        ]);
    }
    
     public function actionConsulta10(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(*) from (SELECT c1.dorsal FROM (SELECT DISTINCT etapa.dorsal FROM  etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL)c1 LEFT JOIN (SELECT DISTINCT etapa.dorsal FROM  etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa)c2 ON c1.dorsal=c2.dorsal WHERE c2.dorsal IS NULL)c1" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT c1.dorsal as dorsal FROM (SELECT DISTINCT etapa.dorsal FROM  etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL)c1 LEFT JOIN (SELECT DISTINCT etapa.dorsal FROM  etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa)c2 ON c1.dorsal=c2.dorsal WHERE c2.dorsal IS NULL" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puertos.",
            "sql"=>"SELECT c1.dorsal FROM (SELECT DISTINCT etapa.dorsal FROM  etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL)c1 LEFT JOIN (SELECT DISTINCT etapa.dorsal FROM  etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa)c2 ON c1.dorsal=c2.dorsal WHERE c2.dorsal IS NULL",
        ]);
    } 
  

      public function actionConsulta10b(){
        
        $r=Etapa::find()->innerJoin('puerto','etapa.numetapa=puerto.numetapa')->distinct()->select("etapa.dorsal");
        
        $dataProvider=new ActiveDataProvider([
            'query'=>Etapa::find()->distinct()->select('dorsal')->leftJoin(['c1'=>$r],"etapa.dorsal=c1.dorsal")->select("etapa.dorsal")->where("c1.dorsal is  null") ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
        
        return $this->render("listView",[
           "dataProvider" => $dataProvider,
        ]);
    }
    
            public function actionConsulta10c(){
        // mediante active record
        $c1=Etapa::find()->joinWith('puertos',true)->distinct()->select("etapa.dorsal")->where("puerto.numetapa IS NULL");
        $c2=Etapa::find()->innerJoin('puerto','etapa.numetapa=puerto.numetapa')->distinct()->select("etapa.dorsal");
        
        $consul=new \yii\db\Query();
        
        $dataProvider=new ActiveDataProvider([
            'query'=>$consul->from(['c1'=>$c1])->leftJoin(['c2'=>$c2],"c1.dorsal=c2.dorsal")->select("c1.dorsal")->where("c2.dorsal is  null") ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
  
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puertos.",
            "sql"=>"SELECT c1.dorsal FROM (SELECT DISTINCT etapa.dorsal FROM  etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS NULL)c1 LEFT JOIN (SELECT DISTINCT etapa.dorsal FROM  etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa)c2 ON c1.dorsal=c2.dorsal WHERE c2.dorsal IS NULL",
        ]);
    }
    
    
    
}
